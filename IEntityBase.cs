﻿namespace StudioKit.Repository.Interfaces
{
	public interface IEntityBase
	{
		int GetId();
	}
}