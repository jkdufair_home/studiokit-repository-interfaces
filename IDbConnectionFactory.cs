﻿using System;
using System.Data;

namespace StudioKit.Repository.Interfaces
{
	public interface IDbConnectionFactory : IDisposable
	{
		IDbConnection Connection { get; }
		IDbConnection InnerConnection { get; }
		IDbConnection InnerInnerConnection { get; }
	}
}