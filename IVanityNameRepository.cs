﻿namespace StudioKit.Repository.Interfaces
{
	public interface IVanityNameRepository<T>
	{
		/// <summary>
		/// Get an entity by VanityName.
		/// </summary>
		/// <param name="vanityName">The vanity name</param>
		/// <returns></returns>
		T GetByVanityName(string vanityName);

		/// <summary>
		/// Returns a new available VanityName.
		/// Checks whether the given vanityName exists and appends a number suffix until the name is available.
		/// </summary>
		/// <param name="vanityName">The vanity name</param>
		/// <returns></returns>
		string FirstAvailableVanityName(string vanityName);

		/// <summary>
		/// Returns a new available VanityName.
		/// Checks whether the given vanityName exists on an entity other than the given one. If so, appends a number suffix until the name is available.
		/// </summary>
		/// <param name="entity">The entity</param>
		/// <param name="vanityName">The vanity name</param>
		/// <returns></returns>
		string FirstAvailableVanityName(T entity, string vanityName);

		/// <summary>
		/// Checks whether the given vanityName exists.
		/// </summary>
		/// <param name="vanityName">The vanity name</param>
		/// <returns></returns>
		bool VanityNameTaken(string vanityName);

		/// <summary>
		/// Checks whether the given vanityName exists on an entity other than the given one. If so, appends a number suffix until the name is available.
		/// </summary>
		/// <param name="entity">The entity</param>
		/// <param name="vanityName">The vanity name</param>
		/// <returns></returns>
		bool VanityNameTaken(T entity, string vanityName);
	}
}