﻿using System;
using System.Data.Linq;

namespace StudioKit.Repository.Interfaces
{
	public interface IDataContextFactory : IDisposable
	{
		/// <summary>
		/// Get the factory
		/// </summary>
		/// <returns></returns>
		DataContext Context { get; }
	}
}