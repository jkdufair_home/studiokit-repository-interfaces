﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace StudioKit.Repository.Interfaces
{
	public interface IRepository<T>
	{
		/// <summary>
		///     Add an entity to the repo
		/// </summary>
		/// <param name="entity">The entity to add</param>
		/// <returns>The result of database being modified by any inserts, updates, or deletes.</returns>
		void Add(T entity);

		/// <summary>
		///     Delete an entity from the repo
		/// </summary>
		/// <param name="entity">The entity to delete</param>
		/// <returns>The result of database being modified by any inserts, updates, or deletes.</returns>
		bool Delete(T entity);

		/// <summary>
		///     Delete collection of entities from the repo
		/// </summary>
		/// <param name="entities">The entities to delete</param>
		/// <returns>The result of database being modified by any inserts, updates, or deletes.</returns>
		bool Delete(IEnumerable<T> entities);

		/// <summary>
		///     Get an entity by ID.  Depends on the entity having a single, integer primary key.  Otherwise go write your own,
		///     now, you hear?
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		T GetById(int id);

		T GetByIdOrDefault(int id);

		bool ExistsById(int id);

		bool Any(Expression<Func<T, bool>> exp);

		/// <summary>
		///     Get all entities from the repo
		/// </summary>
		/// <returns>A collection of all entities</returns>
		IQueryable<T> GetAll();

		/// <summary>
		///     Return all instances of type T that match the expression exp.
		/// </summary>
		/// <param name="exp"></param>
		/// <returns></returns>
		IQueryable<T> FindAll(Expression<Func<T, bool>> exp);

		/// <summary>
		///     Returns the single entity matching the expression.
		///     Throws an exception if there is not exactly one such entity.
		/// </summary>
		/// <param name="exp"></param>
		/// <returns></returns>
		T Single(Expression<Func<T, bool>> exp);

		/// <summary>Returns the first element satisfying the condition.</summary>
		/// <param name="exp"></param>
		/// <returns></returns>
		T First(Expression<Func<T, bool>> exp);

		/// <summary>
		///     Returns the first element or default satisfying the condition.
		/// </summary>
		/// <param name="exp"></param>
		/// <returns></returns>
		T FirstOrDefault(Expression<Func<T, bool>> exp);

		/// <summary>
		///     Returns whether the element exists in the repository.  Implementations will need to consider identity
		/// </summary>
		/// <param name="entity">The entity for which existence is in question</param>
		/// <returns>Whether the entity exists</returns>
		bool Exists(T entity);

		T Create();

		/// <summary>
		///     Saves the changes to the database. Don't throw on error
		/// </summary>
		/// <returns>The result of database being modified by any inserts, updates, or deletes.</returns>
		bool Save();
	}
}