﻿using System;

namespace StudioKit.Repository.Interfaces
{
	public interface ILastModifiedDate
	{
		DateTime LastModifiedDate { get; }
	}
}